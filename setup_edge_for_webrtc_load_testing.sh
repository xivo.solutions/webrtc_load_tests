#!/bin/bash 

WORKER_CONNECTIONS=4096

function set_nginx_load_env_vars() {
  if ! grep -q "_BURST" .env || ! grep -q "_RATE" .env || ! grep -q "WORKER_" .env; then
    echo "Adding env vars in .env file..."
    echo "CTI_BURST=1000" >> ./.env
    echo "LOAD_REQUESTS_RATE=2000" >> ./.env
    echo "LOGIN_BURST=1000" >> ./.env
  else
    echo "Env vars are already set in .env file:"
    cat .env | grep "CTI_BURST"
    cat .env | grep "LOAD_REQUESTS_RATE"
    cat .env | grep "LOGIN_BURST"
  fi
}

function create_nginx_load_template() {
  docker cp edge_nginx_1:/etc/nginx/nginx.conf /etc/docker/edge/nginx.conf
  docker cp edge_nginx_1:/etc/nginx/templates/edge-proxy.conf.template /etc/docker/edge/edge-proxy.conf.template
  if [ -f "/etc/docker/edge/edge-proxy.conf.template" ] || [ -f "/etc/docker/edge/nginx.conf" ]; then
    # Here we add more env vars to the template to suite the load tests.
    echo "Overriding edge-proxy.conf.template to allow higher burts & request rates."
    sed -i 's/zone=auth:20m rate=20r\/m;/zone=auth:20m rate=${LOAD_REQUESTS_RATE}r\/m;/' ./edge-proxy.conf.template
    sed -i '/\/xuc\/api\/2\.0\/cti/{n;s/burst=12/burst=${CTI_BURST}/}' ./edge-proxy.conf.template
    sed -i '/\/xuc\/api\/2\.0\/auth\/login/{n;s/burst=12/burst=${LOGIN_BURST}/}' ./edge-proxy.conf.template

    # Here we directly override the number of the main nginx.conf.
    echo "Creating load nginx.conf.template file that enables more worker_connections." 
    sed -i "s/worker_connections  1024;/worker_connections  ${WORKER_CONNECTIONS};/" ./nginx.conf
  else
    echo "Failed to fetch nginx config files from the container."
    exit 1
  fi
}

function create_nginx_yaml_override() {
  if [ ! -f "nginx-edge.override.yml" ]; then
    echo "Creating edge-nginx yaml override file." 
    cat > ./nginx-edge.override.yml << EOF
version: "3.7"

services:
  nginx:
    environment:
    - CTI_BURST
    - LOAD_REQUESTS_RATE
    - LOGIN_BURST
    volumes:
    - /etc/docker/edge/edge-proxy.conf.template:/etc/nginx/templates/edge-proxy.conf.template
    - /etc/docker/edge/nginx.conf:/etc/nginx/nginx.conf

EOF
    chmod 664 nginx-edge.override.yml
  else
    echo "Edge-nginx yaml override file exists."
  fi
}

function add_nginx_yaml_override_to_dcomp() {
  if  ! grep -q "nginx-edge.override.yml" ~/.bashrc; then
    echo "Adding edge-nginx yaml override into edge-dcomp alias."
    sed -i 's/nginx-edge.yml /nginx-edge.yml -f \/etc\/docker\/edge\/nginx-edge\.override\.yml /' ~/.bashrc
  else
    echo "Edge-nginx yaml override is present in edge-dcomp alias."
  fi
}

set_nginx_load_env_vars
create_nginx_load_template
create_nginx_yaml_override
add_nginx_yaml_override_to_dcomp
echo "Almost ready for load tests. Please run these commands before launching the load:"
echo "source ~/.bashrc"
echo "edge-dcomp up -d"